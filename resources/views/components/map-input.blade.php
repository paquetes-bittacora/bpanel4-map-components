@pushonce('styles')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
          integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI="
          crossorigin=""/>
@endpushonce

<div class="form-group form-row">
    <div class="col-sm-3 col-form-label text-sm-right">
        <label for="id-form-field-1" class="mb-0">{{ $fieldLabel }}</label>
    </div>
    <div class="col-sm-9">
        <div class="input-group m-b">
            @if($description !== '')
                <small class="pt-2 mb-2"><em>{{ $description }}</em></small>
            @endif
            <div id="{{ $name }}" class="map-{{$name}}"></div>
            <input type="hidden" readonly name="{{ $name }}[latitude]" id="{{ $name }}_latitude" value="{{ $latitude }}">
            <input type="hidden" readonly name="{{ $name }}[longitude]" id="{{ $name }}_longitude" value="{{ $longitude }}">
            <input type="hidden" readonly name="{{ $name }}[zoom]" id="{{ $name }}_zoom" value="{{ $zoom }}">
        </div>
    </div>
</div>

<style>
    .map-{{$name}}      {
        min-height: 500px;
        width: 100%;
    }
</style>


@pushonce('scripts')
    <script>
      (() => {
        document.addEventListener('DOMContentLoaded', function () {
          const latitudeInput = document.getElementById('{{ $name }}_latitude')
          const longitudeInput = document.getElementById('{{ $name }}_longitude')
          const zoomInput = document.getElementById('{{ $name }}_zoom')
          const map = L.map('{{ $name }}').setView([{{ $latitude }}, {{ $longitude }}], {{ $zoom }});
          const markersGroup = L.layerGroup().addTo(map);

          // Buscador
          const searchControl = L.esri.Geocoding.geosearch().addTo(map);
          const results = L.layerGroup().addTo(map);

          L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
          }).addTo(map);

          map.addEventListener('click', addMarker);
          map.addEventListener('zoomend', zoomChanged);
          map.addLayer(markersGroup);
          addMarker({latlng: {lat: {{ $latitude }}, lng: {{ $longitude }}}}); // Marcador inicial

          /**
           * Añadir marcador al mapa al hacer click
           */
          function addMarker(e) {
            markersGroup.clearLayers(); // Quitar el marcador anterior
            new L.marker(e.latlng).addTo(markersGroup);

            latitudeInput.value = e.latlng.lat;
            longitudeInput.value = e.latlng.lng;
            results.clearLayers();
          }

          function zoomChanged() {
            zoomInput.value = map.getZoom();
          }

          // Buscador
          searchControl.on('results', function (data) {
            results.clearLayers();
            for (let i = data.results.length - 1; i >= 0; i--) {
              L.marker(data.results[i].latlng).addTo(results);
            }
          });
        })
      })();
    </script>
@endpushonce

@pushonce('scripts')
    <script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
            integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM="
            crossorigin=""></script>

    <script src="https://unpkg.com/esri-leaflet@2.4.1/dist/esri-leaflet.js"
            integrity="sha512-xY2smLIHKirD03vHKDJ2u4pqeHA7OQZZ27EjtqmuhDguxiUvdsOuXMwkg16PQrm9cgTmXtoxA6kwr8KBy3cdcw=="
            crossorigin=""></script>

    <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
          integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
          crossorigin="">
    <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
            integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
            crossorigin=""></script>
@endpushonce
