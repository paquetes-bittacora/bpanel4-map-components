@pushonce('styles')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
          integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI="
          crossorigin=""/>
@endpushonce

<div id="{{ $name }}" class="map-{{$name}}"></div>

<style>
    .map-{{$name}}        {
        min-height: 300px;
        width: 100%;
    }
</style>


@pushonce('scripts')
    <script>
      (() => {
        document.addEventListener('DOMContentLoaded', function () {
          const map = L.map('{{ $name }}').setView([{{ $latitude }}, {{ $longitude }}], {{ $zoom }});
          const markersGroup = L.layerGroup().addTo(map);

          L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
          }).addTo(map);

          map.addLayer(markersGroup);
          new L.marker({lat: {{ $latitude }}, lng: {{ $longitude }}}).addTo(markersGroup);
        })
      })();
    </script>
@endpushonce

@pushonce('scripts')
    <script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
            integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM="
            crossorigin=""></script>

    <script src="https://unpkg.com/esri-leaflet@2.4.1/dist/esri-leaflet.js"
            integrity="sha512-xY2smLIHKirD03vHKDJ2u4pqeHA7OQZZ27EjtqmuhDguxiUvdsOuXMwkg16PQrm9cgTmXtoxA6kwr8KBy3cdcw=="
            crossorigin=""></script>

    <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
          integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
          crossorigin="">
    <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
            integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
            crossorigin=""></script>
@endpushonce
