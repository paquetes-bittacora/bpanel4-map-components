<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\MapComponents\Http\BladeComponents;

use Illuminate\View\Component;

final class Bpanel4MapInput extends Component
{
    public function __construct(
        public string $fieldLabel = 'Mapa',
        public string $name = '',
        public float $latitude = 38.88,
        public float $longitude = -6.97,
        public int $zoom = 12,
        public string $description = ''
    ) {
    }

    public function render()
    {
        return $this->view('bpanel4-map-components::components.map-input');
    }
}
