<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\MapComponents;

use Bittacora\Bpanel4\MapComponents\Http\BladeComponents\Bpanel4Map;
use Bittacora\Bpanel4\MapComponents\Http\BladeComponents\Bpanel4MapInput;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;

final class Bpanel4MapComponentsServiceProvider extends ServiceProvider
{
    public function boot(BladeCompiler $blade): void
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'bpanel4-map-components');
        $blade->component('bpanel4-map-input', Bpanel4MapInput::class);
        $blade->component('bpanel4-map', Bpanel4Map::class);
    }
}
