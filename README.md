# bPanel4 Map components

Componentes para trabajar con mapas en bPanel 4.

## Componente para formularios del panel de control

Con este componente se mostrará un mapa en el que se puede hacer click para fijar una ubicación. Tiene 3 campos `hidden` que contienen la latitud, longitud y zoom. Este componente no se encarga de almacenar estos datos, solo de mostrar el mapa, pero se le pueden pasar parámetros para establecer la ubicación inicial:

```html
<x-bpanel4-map-input 
	name="location"
	description="Texto de ayuda al usuario"
	:latitude="$contact->latitude"
	:longitude="$contact->longitude"
	:zoom="$contact->zoom" />
``` 
